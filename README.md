## JSONB Entity Storage

### Installation

1. Enable module
2. Add the following to `services.yml`:
    ```yaml
    services:
      config.storage: @config.storage.jsonb
    ```
