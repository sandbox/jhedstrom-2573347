<?php

/**
 * @file
 * Contains \Drupal\jsonb_storage\Entity\ConfigStorage.
 */

namespace Drupal\jsonb_storage\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\DatabaseStorage;

/**
 * A JSONB-based configuration storage.
 */
class ConfigStorage extends DatabaseStorage {

  /**
   * {@inheritdoc}
   *
   * Override the schema to use jsonb for the data column.
   */
  protected static function schemaDefinition() {
    $schema = parent::schemaDefinition();
    $schema['fields']['data']['pgsql_type'] = 'jsonb';
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function encode($data) {
    return Json::encode($data);
  }

  /**
   * {@inheritdoc}
   */
  public function decode($raw) {
    return Json::decode($raw);
  }

}
