<?php

/**
 * @file
 * Contains \Drupal\jsonb_storage\Entity\NodeStorage.
 */

namespace Drupal\jsonb_storage\Entity;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;

/**
 * Node storage.
 */
class NodeStorage extends ContentEntityStorage implements NodeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(NodeInterface $node) {

  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {

  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(NodeInterface $node) {

  }

  /**
   * {@inheritdoc}
   */
  public function updateType($old_type, $new_type) {

  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {

  }

}
