<?php

/**
 * @file
 * Contains \Drupal\jsonb_storage\Entity\ContentEntityStorage.
 */

namespace Drupal\jsonb_storage\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * A JSONB-based content entity storage.
 */
class ContentEntityStorage extends ContentEntityStorageBase {

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(array $ids = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doDelete($entities) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity) {

  }

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'entity.query.sql';
  }

  /**
   * {@inheritdoc}
   */
  public function countFieldData($storage_definition, $as_bool = FALSE) {

  }

  /**
   * {@inheritdoc}
   */
  protected function readFieldItemsToPurge(FieldDefinitionInterface $field_definition, $batch_size) {

  }

  /**
   * {@inheritdoc}
   */
  protected function purgeFieldItems(ContentEntityInterface $entity, FieldDefinitionInterface $field_definition) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadRevisionFieldItems($revision_id) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doSaveFieldItems(ContentEntityInterface $entity, array $names = []) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteFieldItems($entities) {

  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteRevisionFieldItems(ContentEntityInterface $revision) {

  }

}
