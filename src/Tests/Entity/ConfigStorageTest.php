<?php

/**
 * @file
 * Contains \Drupal\jsonb_storage\Tests\Entity\ConfigStorageTest.
 */

namespace Drupal\jsonb_storage\Tests\Entity;

use Drupal\Component\Serialization\Json;
use Drupal\config\Tests\Storage\DatabaseStorageTest;
use Drupal\jsonb_storage\Entity\ConfigStorage;

/**
 * Tests ConfigStorage controller operations.
 *
 * @group jsonb_storage
 */
class ConfigStorageTest extends DatabaseStorageTest {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['jsonb_storage'];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->storage = new ConfigStorage($this->container->get('database'), 'config_jsonb');
    $this->invalidStorage = new ConfigStorage($this->container->get('database'), 'invalid');

    // This will create the table.
    $this->storage->write('system.performance', ['foo' => 'bar']);
  }

  protected function read($name) {
    $data = db_query('SELECT data FROM {config_jsonb} WHERE name = :name', array(':name' => $name))->fetchField();
    return Json::decode($data);
  }

  protected function insert($name, $data) {
    db_insert('config_jsonb')->fields(['name' => $name, 'data' => Json::encode($data)])->execute();
  }

  protected function update($name, $data) {
    db_update('config_jsonb')->fields(['data' => Json::encode($data)])->condition('name', $name)->execute();
  }

  protected function delete($name) {
    db_delete('config_jsonb')->condition('name', $name)->execute();
  }

}
